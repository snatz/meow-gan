from glob import glob
import cv2
from math import atan2, pi, cos, sin


CAT_DATASET_PATH = "cats_dataset\\"
CAT_DATASET_PATH_64x64 = "cats_64x64\\"
CAT_DATASET_PATH_128x128 = "cats_128x128\\"


def rotate_coords(features_coords, eyes_center, eyes_angle_rads):
    eyes_angle_rads = -eyes_angle_rads
    rotated_features_coords = []
    xs, ys = features_coords[::2], features_coords[1::2]  # Extract the x and y positions

    n = min(len(xs), len(ys))
    i = 0
    cos_angle = cos(eyes_angle_rads)
    sin_angle = sin(eyes_angle_rads)
    eyes_center_x = eyes_center[0]
    eyes_center_y = eyes_center[1]

    # Apply rotation
    while i < n:
        offset_x = xs[i] - eyes_center_x
        offset_y = ys[i] - eyes_center_y
        rotation_x = offset_x * cos_angle - offset_y * sin_angle + eyes_center_x
        rotation_y = offset_x * sin_angle + offset_y * cos_angle + eyes_center_y

        rotated_features_coords += [rotation_x, rotation_y]
        i += 1

    return rotated_features_coords


def refine_cat(cat_image, features_coords):
    left_eye_x, left_eye_y = features_coords[0], features_coords[1]
    right_eye_x, right_eye_y = features_coords[2], features_coords[3]
    nose_x = features_coords[4]

    if left_eye_x > right_eye_x and left_eye_y > right_eye_y and nose_x > right_eye_x:
        # The face is reversed from the viewer's perspective => swap coordinates in order to simplify the rotation
        left_eye_x, right_eye_x = right_eye_x, left_eye_x
        left_eye_y, right_eye_y = right_eye_y, left_eye_y

    eyes_center_x = 0.5 * (left_eye_x + right_eye_x)
    eyes_center_y = 0.5 * (left_eye_y + right_eye_y)
    eyes_center = (eyes_center_x, eyes_center_y)

    eyes_delta_x = right_eye_x - left_eye_x
    eyes_delta_y = right_eye_y - left_eye_y

    eyes_angle_rads = atan2(eyes_delta_y, eyes_delta_x)
    eyes_angle_degrees = eyes_angle_rads * 180.0 / pi

    # Rotate the image in order to have the cat facing straight the camera
    rotation = cv2.getRotationMatrix2D(eyes_center, eyes_angle_degrees, 1.0)
    image_size = cat_image.shape[1::-1]
    cat_image_straight = cv2.warpAffine(cat_image, rotation, image_size, borderValue=(128, 128, 128))  # Filling with gray

    # The features coords have to change as well
    rotated_features_coords = rotate_coords(features_coords, eyes_center, eyes_angle_rads)

    # See cat images features documentation
    new_image_width = abs(rotated_features_coords[16] - rotated_features_coords[6])
    new_image_height = new_image_width  # Square image

    min_x = eyes_center_x - new_image_width/2
    min_y = eyes_center_y - new_image_height*2/5

    if min_x < 0:
        new_image_width += min_x
        min_x = 0

    if min_y < 0:
        new_image_height += min_y
        min_y = 0

    # Crop the cat's face
    refined_cat = cat_image_straight[int(min_y):int(min_y + new_image_height), int(min_x):int(min_x + new_image_width)]
    return refined_cat


def main():
    for cat_image_path in glob(CAT_DATASET_PATH + "*.jpg"):
        features_description = open("%s.cat" % cat_image_path, 'r')
        features_coords = [int(i) for i in features_description.readline().split()[1:]]  # Remove the first feature desc
        cat_image = cv2.imread(cat_image_path)
        refined_cat = refine_cat(cat_image, features_coords)

        if refined_cat is None:
            print("Cat refining process failed for %s." % cat_image_path)
        else:
            height, width, channels = refined_cat.shape

            if min(height, width) >= 64:
                smaller_image_path = cat_image_path.replace(CAT_DATASET_PATH, CAT_DATASET_PATH_64x64)
                cv2.imwrite(smaller_image_path, refined_cat)

            if min(height, width) >= 128:
                bigger_image_path = cat_image_path.replace(CAT_DATASET_PATH, CAT_DATASET_PATH_128x128)
                cv2.imwrite(bigger_image_path, refined_cat)

            print("Refined %s" % cat_image_path)


if __name__ == "__main__":
    main()

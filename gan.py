import os

import numpy as np
from keras import initializers
from keras.layers import Input
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.core import Dense, Dropout
from keras.models import Model, Sequential
from keras.optimizers import Adam
from tqdm import tqdm

import utils

# Keras is using TensorFlow's backend engine
os.environ["KERAS_BACKEND"] = "tensorflow"

# To make sure that we can reproduce the experiment and get the same results
np.random.seed(10)


def get_optimizer():
    return Adam(lr=0.0002, beta_1=0.5)


def get_generator(optimizer):
    generator = Sequential()
    generator.add(Dense(256, input_dim=utils.random_dim, kernel_initializer=initializers.RandomNormal(stddev=0.02)))
    generator.add(LeakyReLU(0.2))

    generator.add(Dense(512))
    generator.add(LeakyReLU(0.2))

    generator.add(Dense(1024))
    generator.add(LeakyReLU(0.2))

    generator.add(Dense(utils.discriminator_input_dim, activation='tanh'))
    generator.compile(loss='binary_crossentropy', optimizer=optimizer)
    return generator


def get_discriminator(optimizer):
    discriminator = Sequential()
    discriminator.add(Dense(1024,
                            input_dim=utils.discriminator_input_dim,
                            kernel_initializer=initializers.RandomNormal(stddev=0.02)))
    discriminator.add(LeakyReLU(0.2))
    discriminator.add(Dropout(0.3))

    discriminator.add(Dense(512))
    discriminator.add(LeakyReLU(0.2))
    discriminator.add(Dropout(0.3))

    discriminator.add(Dense(256))
    discriminator.add(LeakyReLU(0.2))
    discriminator.add(Dropout(0.3))

    discriminator.add(Dense(1, activation='sigmoid'))
    discriminator.compile(loss='binary_crossentropy', optimizer=optimizer)
    return discriminator


def get_gan_network(discriminator, rand_dim, generator, optimizer):
    # We initially set trainable to False since we only want to train either the
    # generator or discriminator at a time
    discriminator.trainable = False

    # gan input (noise) as a random vector
    gan_input = Input(shape=(rand_dim,))

    # the output of the generator (an image)
    x = generator(gan_input)

    # get the output of the discriminator (probability if the image is real or not)
    gan_output = discriminator(x)
    gan = Model(inputs=gan_input, outputs=gan_output)
    gan.compile(loss='binary_crossentropy', optimizer=optimizer)

    return gan


def train(epochs=1):
    bs = utils.batch_size
    rd = utils.random_dim

    # Load the dataset
    x_train = utils.load_cat_data()

    # Build our GAN netowrk
    adam = get_optimizer()
    generator = get_generator(adam)
    discriminator = get_discriminator(adam)
    gan = get_gan_network(discriminator, rd, generator, adam)

    for e in range(1, epochs+1):
        print('-'*15, 'Epoch %d' % e, '-'*15)
        for _ in tqdm(range(utils.batch_count)):
            # Get a random set of input noise and images
            noise = np.random.normal(0, 1, size=[bs, utils.random_dim])
            image_batch = x_train[np.random.randint(0, x_train.shape[0], size=bs)]

            # Generate fake images
            generated_images = generator.predict(noise)
            x = np.concatenate([image_batch, generated_images])

            # Labels for generated and real data
            y_dis = np.zeros(2*bs)
            # One-sided label smoothing
            y_dis[:bs] = 0.9

            # Train discriminator
            discriminator.trainable = True
            discriminator.train_on_batch(x, y_dis)

            # Train generator
            noise = np.random.normal(0, 1, size=[bs, rd])
            y_gen = np.ones(bs)
            discriminator.trainable = False
            gan.train_on_batch(noise, y_gen)

        if e == 1 or e % 20 == 0:
            utils.plot_generated_images(e, generator)


if __name__ == '__main__':
    train(epochs=utils.epochs_count)

import numpy as np
from glob import glob1
import os
from tqdm import tqdm
import cv2


# Original images path
path = "cats_original\\"

# Image output size
image_output_size = 64

# npy filename
filename = "cats_original_" + str(image_output_size) + "x" + str(image_output_size)

# Every image path
image_files = glob1(path, "*.jpg")


for i in tqdm(range(len(image_files))):
    im = cv2.imread(os.path.join(path, image_files[i]))
    im_rsz = cv2.resize(im, (image_output_size, image_output_size), cv2.INTER_CUBIC)
    data = np.array([im_rsz], dtype="uint8")

    if i == 0:
        images_array = data
    else:
        images_array = np.concatenate((images_array, data))

np.save(filename + ".npy", images_array)

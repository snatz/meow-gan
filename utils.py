import os

import numpy as np
from matplotlib import pyplot as plt
from glob import glob1

# The dimension of our random noise vector.
random_dim = 100

# Cats images path
cats_path = "cats_128x128\\"

# Cats archive
cats_archive = "cats_128x128.npy"

# Images' batch size
batch_size = 128

# Cats images count
images_count = len(glob1(cats_path, "*.jpg"))

# Number of batches needed
batch_count = images_count // batch_size

# Input images' size
input_image_size = 128

# Output images' size
output_image_size = 128

# Image's color dimension
color_dim = 3

# Discriminator's input dimension
discriminator_input_dim = input_image_size * input_image_size * color_dim

# Discriminator's input shape
discriminator_input_shape = (input_image_size, input_image_size, color_dim)

# Epochs count
epochs_count = 1000

# Folder where generated images are saved
epoch_generated_images_path_format_folder = "generations_cat_128x128_simple_gan\\"

# Name format of images saved
epoch_generated_images_path_format = epoch_generated_images_path_format_folder + 'gan_generated_image_epoch_%d.png'


def load_cat_data():
    # Load images batch
    data = np.load(cats_archive)

    # normalize our inputs to be in the range[-1, 1]
    data = (data.astype(np.float32) - 127.5) / 127.5

    # convert batch with a shape of (batch_size, input_size, input_size, color_dim) to (b_s, i_s * i_s * c_d)
    data = data.reshape(images_count, input_image_size * input_image_size * color_dim)

    return data


# Create a wall of generated images
def plot_generated_images(epoch, generator, examples=100, dim=(10, 10), figsize=(40, 40)):
    noise = np.random.normal(0, 1, size=[examples, random_dim])
    generated_images = generator.predict(noise)
    generated_images = generated_images.reshape((examples, output_image_size, output_image_size, color_dim))
    generated_images = ((generated_images + 127.5) * 127.5).astype(np.uint8)

    plt.figure(figsize=figsize)
    for i in range(generated_images.shape[0]):
        plt.subplot(dim[0], dim[1], i+1)
        plt.imshow(generated_images[i], interpolation='nearest')
        plt.axis('off')

    plt.tight_layout()

    if not os.path.isdir(epoch_generated_images_path_format_folder):
        os.makedirs(epoch_generated_images_path_format_folder)

    plt.savefig(epoch_generated_images_path_format % epoch)

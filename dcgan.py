import os

import numpy as np
from keras.layers import Input, Dense, Reshape, Flatten, Dropout, BatchNormalization, Activation, ZeroPadding2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras.models import Model, Sequential
from keras.optimizers import Adam
from tqdm import tqdm

import utils

# Keras is using TensorFlow's backend engine
os.environ["KERAS_BACKEND"] = "tensorflow"

# To make sure that we can reproduce the experiment and get the same results
np.random.seed(10)


def get_optimizer():
    return Adam(lr=0.0002, beta_1=0.5)


def get_generator():
    generator = Sequential()

    # 16 in order to ultimately generate 64x64 images
    generator.add(Dense(128 * 16 * 16, activation="relu", input_dim=utils.random_dim))
    generator.add(Reshape((16, 16, 128)))
    generator.add(UpSampling2D())

    generator.add(Conv2D(128, kernel_size=3, padding="same"))
    generator.add(BatchNormalization(momentum=0.8))
    generator.add(Activation("relu"))
    generator.add(UpSampling2D())

    generator.add(Conv2D(64, kernel_size=3, padding="same"))
    generator.add(BatchNormalization(momentum=0.8))
    generator.add(Activation("relu"))

    generator.add(Conv2D(utils.color_dim, kernel_size=3, padding="same"))
    generator.add(Activation("tanh"))

    #generator.summary()

    noise = Input(shape=(utils.random_dim,))
    img = generator(noise)

    return Model(noise, img)


def get_discriminator():
    discriminator = Sequential()
    discriminator.add(Conv2D(32, kernel_size=3, strides=2, input_shape=utils.discriminator_input_shape, padding="same"))
    discriminator.add(LeakyReLU(0.2))
    discriminator.add(Dropout(0.25))

    discriminator.add(Conv2D(64, kernel_size=3, strides=2, padding="same"))
    discriminator.add(ZeroPadding2D(padding=((0,1),(0,1))))
    discriminator.add(BatchNormalization(momentum=0.8))
    discriminator.add(LeakyReLU(0.2))
    discriminator.add(Dropout(0.25))

    discriminator.add(Conv2D(128, kernel_size=3, strides=2, padding="same"))
    discriminator.add(BatchNormalization(momentum=0.8))
    discriminator.add(LeakyReLU(0.2))
    discriminator.add(Dropout(0.25))

    discriminator.add(Conv2D(256, kernel_size=3, strides=1, padding="same"))
    discriminator.add(BatchNormalization(momentum=0.8))
    discriminator.add(LeakyReLU(0.2))
    discriminator.add(Dropout(0.25))

    discriminator.add(Flatten())
    discriminator.add(Dense(1, activation='sigmoid'))

    #discriminator.summary()

    img = Input(shape=utils.discriminator_input_shape)
    validity = discriminator(img)
    
    return Model(img, validity)


def get_gan_network(discriminator, rand_dim, generator, optimizer):
    # We initially set trainable to False since we only want to train either the
    # generator or discriminator at a time
    discriminator.trainable = False

    # gan input (noise) as a random vector
    gan_input = Input(shape=(rand_dim,))

    # the output of the generator (an image)
    x = generator(gan_input)

    # get the output of the discriminator (probability if the image is real or not)
    gan_output = discriminator(x)
    gan = Model(gan_input, gan_output)
    gan.compile(loss='binary_crossentropy', optimizer=optimizer)

    return gan


def train(epochs=1):
    bs = utils.batch_size
    rd = utils.random_dim

    # Load the dataset
    x_train = utils.load_cat_data()

    # Build our GAN netowrk
    rms = get_optimizer()
    generator = get_generator()
    discriminator = get_discriminator()
    discriminator.compile(loss='binary_crossentropy', optimizer=rms)

    gan = get_gan_network(discriminator, rd, generator, rms)

    for e in range(1, epochs+1):
        print('-'*15, 'Epoch %d' % e, '-'*15)
        for _ in tqdm(range(utils.batch_count)):
            # Get a random set of input noise and images
            noise = np.random.normal(0, 1, size=[bs, utils.random_dim])
            image_batch = x_train[np.random.randint(0, x_train.shape[0], size=bs)]
            image_batch = np.reshape(image_batch, (bs, utils.input_image_size, utils.input_image_size, utils.color_dim))

            # Generate fake images
            generated_images = generator.predict(noise)
            x = np.concatenate([image_batch, generated_images])

            # Labels for generated and real data
            y_dis = np.zeros(2*bs)
            # One-sided label smoothing
            y_dis[:bs] = 0.9

            # Train discriminator
            discriminator.trainable = True
            discriminator.train_on_batch(x, y_dis)

            # Train generator
            noise = np.random.normal(0, 1, size=[bs, rd])
            y_gen = np.ones(bs)
            discriminator.trainable = False
            gan.train_on_batch(noise, y_gen)

        if e == 1 or e % 20 == 0:
            utils.plot_generated_images(e, generator)


if __name__ == '__main__':
    train(epochs=utils.epochs_count)
